package com.wt.sample.ui;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.wt.sample.R;
import com.wt.sample.utill.Utill;
import com.wt.sdk.ConnectionListener;
import com.wt.sdk.MediaConfiguration;
import com.wt.sdk.Participant;
import com.wt.sdk.ParticipantType;
import com.wt.sdk.ReconnectListener;
import com.wt.sdk.Session;
import com.wt.sdk.SessionError;
import com.wt.sdk.SessionListener;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements SessionListener,
    ReconnectListener, ConnectionListener, PublishersAdapter.SwitchCamCallback {

    private Session mSession;
    private PublishersAdapter mPublisherAdapter;
    private ParticipantsAdapter mParticipantAdapter;
    private String mDisplayName = "", mToken = "";
    private AppCompatTextView mAudioQos, mVideoQos, mMode, mSessionTimeDuration, mConnectionState;
    private SwitchCompat mSwitchMode;

    private Timer mReconnectTimer = null;
    private final Timer mDurationTimer = new Timer("MainActivityCountThread");
    private final Timer mConnectionStateTimer = new Timer("MainActivityCountThread");
    private final Handler mHandler = new Handler();
    private long mSessionTimeDurationMls = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // To prevent screen to be switched off
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        // Extract session's params
        if (getIntent() != null) {
            if (getIntent().getExtras() != null) {
                mDisplayName = String.valueOf(getIntent().getExtras().get(StartActivity.DISPLAY_NAME_CODE));
            }
        }

        initViewsAndListeners();
        initSession();
    }

    private void initViewsAndListeners() {
        final AppCompatButton btnStartCameraPreview = findViewById(R.id.btn_cam_preview);
        final AppCompatButton btnConnectSessionFP = findViewById(R.id.btn_connect_session_fp);
        final AppCompatButton btnConnectSessionAVB = findViewById(R.id.btn_connect_session_avb);
        final AppCompatButton btnConnectSessionAB = findViewById(R.id.btn_connect_session_ab);
        final AppCompatButton btnConnectWatching = findViewById(R.id.btn_connect_watching);
        final AppCompatButton btnAdjustVideoQuality = findViewById(R.id.btn_adjust_frame_rate);
        final AppCompatButton btnFPType = findViewById(R.id.btn_pt_fp);
        final AppCompatButton btnAVBType = findViewById(R.id.btn_pt_avb);
        final AppCompatTextView txtWidth = findViewById(R.id.txt_width_value);
        final AppCompatTextView txtHeight = findViewById(R.id.txt_height_value);
        final AppCompatTextView txtFrameRate = findViewById(R.id.txt_frame_rate_value);
        mAudioQos = findViewById(R.id.txt_audio_qos_value);
        mVideoQos = findViewById(R.id.txt_video_qos_value);
        mMode = findViewById(R.id.txt_service_mode_value);
        mSwitchMode = findViewById(R.id.switch_service_mode);
        mSessionTimeDuration = findViewById(R.id.txt_session_time_duration);
        mConnectionState = findViewById(R.id.txt_connection_state);
        final SeekBar widthSeek = findViewById(R.id.seek_width);
        final SeekBar heightSeek = findViewById(R.id.seek_height);
        final SeekBar frameSeek = findViewById(R.id.seek_frame_rate);
        setParticipantsTypesSheet();
        txtWidth.setText(String.valueOf(widthSeek.getProgress()));
        txtHeight.setText(String.valueOf(heightSeek.getProgress()));
        txtFrameRate.setText(String.valueOf(frameSeek.getProgress()));

        mSwitchMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                String txtMode = getString(b ? R.string.txt_mode_mesh : R.string.txt_mode_sfu);
                mMode.setText(txtMode);
            }
        });

        btnStartCameraPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (mSession != null) {
                    btnStartCameraPreview.setVisibility(View.GONE);
                    mSession.setVideoResolution(widthSeek.getProgress(), heightSeek.getProgress());
                    mSession.setVideoFrameRate(frameSeek.getProgress());
                    mSession.startCameraPreview();
                }
            }
        });

        btnConnectSessionFP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (mSession != null) {
                    btnStartCameraPreview.setVisibility(View.GONE);
                    btnConnectSessionFP.setVisibility(View.GONE);
                    btnConnectSessionAVB.setVisibility(View.GONE);
                    btnConnectSessionAB.setVisibility(View.GONE);
                    btnConnectWatching.setVisibility(View.GONE);
                    mSwitchMode.setVisibility(View.GONE);
                    setSessionParams();
                    mSession.setVideoResolution(widthSeek.getProgress(), heightSeek.getProgress());
                    mSession.setVideoFrameRate(frameSeek.getProgress());
                    mSession.connect();
                }
            }
        });

        btnConnectSessionAVB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (mSession != null) {
                    btnStartCameraPreview.setVisibility(View.GONE);
                    btnConnectSessionFP.setVisibility(View.GONE);
                    btnConnectSessionAVB.setVisibility(View.GONE);
                    btnConnectSessionAB.setVisibility(View.GONE);
                    btnConnectWatching.setVisibility(View.GONE);
                    mSwitchMode.setVisibility(View.GONE);
                    setSessionParams();
                    mSession.setVideoResolution(widthSeek.getProgress(), heightSeek.getProgress());
                    mSession.setVideoFrameRate(frameSeek.getProgress());
                    mSession.connectWithAudioVideo();
                }
            }
        });

        btnConnectSessionAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (mSession != null) {
                    btnStartCameraPreview.setVisibility(View.GONE);
                    btnConnectSessionFP.setVisibility(View.GONE);
                    btnConnectSessionAVB.setVisibility(View.GONE);
                    btnConnectSessionAB.setVisibility(View.GONE);
                    btnConnectWatching.setVisibility(View.GONE);
                    mSwitchMode.setVisibility(View.GONE);
                    setSessionParams();
                    mSession.connectWithAudio();
                }
            }
        });


        btnConnectWatching.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSession != null) {
                    btnStartCameraPreview.setVisibility(View.GONE);
                    btnConnectSessionFP.setVisibility(View.GONE);
                    btnConnectSessionAVB.setVisibility(View.GONE);
                    btnConnectSessionAB.setVisibility(View.GONE);
                    btnConnectWatching.setVisibility(View.GONE);
                    mSwitchMode.setVisibility(View.GONE);
                    setSessionParams();
                    mSession.connectAsViewer();
                }
            }
        });

        btnFPType.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View view) {
                BottomSheetBehavior.from(findViewById(R.id.participants_types_sheet)).setState(BottomSheetBehavior.STATE_COLLAPSED);
                if (mSession != null) {
                    mSession.setLocalParticipantType(ParticipantType.FULL_PARTICIPANT);
                }
            }
        });

        btnAVBType.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View view) {
                BottomSheetBehavior.from(findViewById(R.id.participants_types_sheet)).setState(BottomSheetBehavior.STATE_COLLAPSED);
                if (mSession != null) {
                    mSession.setLocalParticipantType(ParticipantType.AV_BROADCASTER);
                }
            }
        });

        btnAdjustVideoQuality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSession != null) {
                    mSession.adjustResolution(widthSeek.getProgress(), heightSeek.getProgress());
                    mSession.adjustFrameRate(frameSeek.getProgress());
                }
            }
        });

        widthSeek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    progress = getResolutionProgress(progress);
                    widthSeek.setProgress(progress);
                    txtWidth.setText(String.valueOf(progress));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        heightSeek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    progress = getResolutionProgress(progress);
                    heightSeek.setProgress(progress);
                    txtHeight.setText(String.valueOf(progress));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        frameSeek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    if (progress < 5) {
                        progress = 1;
                    } else if (progress < 10) {
                        progress = 5;
                    } else if (progress < 15) {
                        progress = 10;
                    } else if (progress < 20) {
                        progress = 15;
                    } else if (progress < 25) {
                        progress = 20;
                    } else if (progress < 30) {
                        progress = 25;
                    } else if (progress < 40) {
                        progress = 30;
                    } else if (progress < 50) {
                        progress = 40;
                    } else if (progress < 60) {
                        progress = 50;
                    } else {
                        progress = 60;
                    }
                    frameSeek.setProgress(progress);
                    txtFrameRate.setText(String.valueOf(progress));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        // Create PublisherAdapter instance
        mPublisherAdapter = new PublishersAdapter(this);
        // Set PublisherAdapter to RecyclerView
        ((RecyclerView) findViewById(R.id.publishers)).setAdapter(mPublisherAdapter);

        // Create ParticipantAdapter instance
        mParticipantAdapter = new ParticipantsAdapter();
        // Set ParticipantAdapter to RecyclerView
        ((RecyclerView) findViewById(R.id.participants)).setAdapter(mParticipantAdapter);
    }

    private void setParticipantsTypesSheet() {
        BottomSheetBehavior.from(findViewById(R.id.participants_types_sheet)).setState(BottomSheetBehavior.STATE_COLLAPSED);
        BottomSheetBehavior.from(findViewById(R.id.participants_types_sheet)).setDraggable(true);

        //click event for show-dismiss bottom sheet
        findViewById(R.id.sheet_header).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (BottomSheetBehavior.from(findViewById(R.id.participants_types_sheet)).getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    BottomSheetBehavior.from(findViewById(R.id.participants_types_sheet)).setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    BottomSheetBehavior.from(findViewById(R.id.participants_types_sheet)).setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            }
        });
        findViewById(R.id.participants_types_sheet).setVisibility(View.GONE);
    }

    private int getResolutionProgress(int progress) {
        if (progress < 144) {
            progress = 120;
        } else if (progress < 160) {
            progress = 144;
        } else if (progress < 176) {
            progress = 160;
        } else if (progress < 240) {
            progress = 176;
        } else if (progress < 288) {
            progress = 240;
        } else if (progress < 320) {
            progress = 288;
        } else if (progress < 352) {
            progress = 320;
        } else if (progress < 360) {
            progress = 352;
        } else if (progress < 480) {
            progress = 360;
        } else if (progress < 600) {
            progress = 480;
        } else if (progress < 640) {
            progress = 600;
        } else if (progress < 720) {
            progress = 640;
        } else if (progress < 768) {
            progress = 720;
        } else if (progress < 800) {
            progress = 768;
        } else if (progress < 1024) {
            progress = 800;
        } else if (progress < 1200) {
            progress = 1024;
        } else if (progress < 1280) {
            progress = 1200;
        } else {
            progress = 1280;
        }
        return progress;
    }

    private void initSession() {
        mSession = new Session.SessionBuilder(this)
            .setReconnectListener(this)
            .setConnectionListener(this)
            .build(this);
    }

    private void setSessionParams() {
        mSession.setDisplayName(mDisplayName);
        mSession.setToken(mToken);
        mSession.setServiceMode(mSwitchMode.isChecked());
        mSession.enableStats();
    }

    @Override
    protected void onPause() {
        super.onPause();
        disconnect();
    }

    @Override
    public void onBackPressed() {
        if (BottomSheetBehavior.from(findViewById(R.id.participants_types_sheet)).getState() == BottomSheetBehavior.STATE_EXPANDED) {
            BottomSheetBehavior.from(findViewById(R.id.participants_types_sheet)).setState(BottomSheetBehavior.STATE_COLLAPSED);
            return;
        }
        disconnect();
        super.onBackPressed();
    }

    private void disconnect() {
        clearAdapters();
        if (mSession != null) {
            mSession.disconnect();
        }
    }

    private void clearAdapters() {
        // Clear participants and disconnect from the session
        if (mParticipantAdapter != null) {
            mParticipantAdapter.clearParticipants();
        }
        if (mPublisherAdapter != null) {
            mPublisherAdapter.clearPublishers();
        }
    }

    @Override
    protected void onDestroy() {
        clearAdapters();
        super.onDestroy();
    }

    @Override
    public void onConnected(final String sessionId, final ArrayList<String> participants) {
        Utill.showMessage(this, "Connected to session with id:\n".concat(sessionId));
        findViewById(R.id.participants_types_sheet).setVisibility(View.VISIBLE);
        mSessionTimeDuration.setVisibility(View.VISIBLE);
        mDurationTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                mSessionTimeDurationMls+=1000L;
                if (isFinishing()) {
                    cancel();
                    return;
                }
                if (mSessionTimeDuration != null) {
                    mHandler.post(() -> mSessionTimeDuration.setText(Utill.formatDurationToHHMMSSSSS(mSessionTimeDurationMls)));
                }
                if (mSession != null) {
                    mSession.sendPlayerData(String.valueOf(mSessionTimeDurationMls));
                }
            }
        }, 0, 1000);
    }

    @Override
    public void onOrganizerReceived() {
        // Method is triggered when user becomes an organizer of the Session.
        Utill.showMessage(this, "You got organizer session's role");
    }

    @Override
    public void onDisconnected() {
        Utill.showError(this, "Disconnected from session");
        finish();
    }

    @Override
    public void onError(final SessionError sessionError) {
        if (sessionError != null && !TextUtils.isEmpty(sessionError.getName())) {
            String errStr = sessionError.getName();
            if (!TextUtils.isEmpty(sessionError.getDescription())) {
                errStr += " " + sessionError.getDescription();
            }
            Utill.showError(this, errStr);
        }
    }

    @Override
    public void onRemoteParticipant(final Participant participant) {
        if (mParticipantAdapter != null) {
            mParticipantAdapter.addParticipant(participant.getName());
        }
    }

    @Override
    public void onParticipantLeft(final Participant participant) {
        if (mParticipantAdapter != null) {
            mParticipantAdapter.removeParticipant(participant.getName());
        }
    }

    @Override
    public void onLocalPublisher(final Participant publisher) {
        if (mPublisherAdapter != null) {
            mPublisherAdapter.addLocalPublisher(publisher);
        }
    }

    @Override
    public void onRemotePublisher(final Participant publisher) {
        if (mPublisherAdapter != null) {
            mPublisherAdapter.addPublisher(publisher);
        }
    }

    @Override
    public void onUpdatePublisher(String participantId, Participant participant) {
        if (mReconnectTimer != null) {
            mReconnectTimer.cancel();
        }
        if (mPublisherAdapter != null) {
            mPublisherAdapter.updatePublisher(participantId, participant);
        }
    }

    @Override
    public void onPublisherLeft(Participant publisher) {
        if (mPublisherAdapter != null) {
            mPublisherAdapter.removePublisher(publisher);
        }
    }

    @Override
    public void onPublisherMediaStateChanged(final String participantId, final MediaConfiguration.MediaType mediaType, final MediaConfiguration.MediaState mediaState) {
        if (mPublisherAdapter != null) {
            mPublisherAdapter.updatePublisherMedia(participantId, mediaType, mediaState);
        }
    }

    @Override
    public void onStatsReceived(final JSONObject stats) {
        if (stats != null) {
            try {
                mAudioQos.setText(String.valueOf(stats.get("audioAvg")).concat(" -> ").concat(String.valueOf(stats.get("audioQosMos"))));
                mVideoQos.setText(String.valueOf(stats.get("videoAvg")).concat(" -> ").concat(String.valueOf(stats.get("videoQosMos"))));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onStateServiceMode(String mode) {
        mMode.setText(mode);
    }

    @Override
    public void onPlayerDataReceived(final String data) {
        // Method is triggered when user gets sync notification from session's organizer.
        Utill.showMessage(this, "Player data received = ".concat(data));
        // Set player speed according delta
    }

    @Override
    public void onSwitchCameraClicked() {
        if (mSession != null) {
            mSession.switchCamera();
        }
    }

    @Override
    public void onLocalParticipantReconnecting() {
        Utill.showMessage(this, "Reconnecting local participant...");
    }

    @Override
    public void onLocalParticipantReconnected() {
        Utill.showMessage(this, "Reconnected local participant...");
    }

    @Override
    public void onRemoteParticipantReconnecting(Participant participant, String participantId) {
        mPublisherAdapter.progressConnection(participantId, true);
        mReconnectTimer = new Timer("MainActivityReconnectThread");
        mReconnectTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(() -> {
                    if (mPublisherAdapter != null) {
                        mPublisherAdapter.removePublisherById(participantId);
                    }
                    if (mParticipantAdapter != null) {
                        mParticipantAdapter.removeParticipant(participant.getName());
                    }
                });
            }
        }, 7000);
    }

    @Override
    public void onRemoteParticipantReconnected(String oldParticipantId, Participant participant) {
        if (mReconnectTimer != null) {
            mReconnectTimer.cancel();
        }
        if (mPublisherAdapter != null) {
            mPublisherAdapter.progressConnection(oldParticipantId, false);
        }
    }

    @Override
    public void onLocalConnectionLost() {
        setConnectionState(R.string.txt_connection_lost, false);
    }

    @Override
    public void onLocalConnectionResumed() {
        setConnectionState(R.string.txt_connection_resumed, true);
    }

    @Override
    public void onRemoteConnectionLost(String participantId) {
        if (mPublisherAdapter != null) {
            mPublisherAdapter.remotePublisherConnectionLost(participantId);
        }
    }

    private void setConnectionState(int strConnectionState, boolean isConnected) {
        int txtColor = isConnected ? R.color.colorGreen : R.color.colorRed;
        mConnectionState.setVisibility(View.VISIBLE);
        mConnectionState.setText(strConnectionState);
        mConnectionState.setBackgroundColor(ContextCompat.getColor(this, txtColor));
        mConnectionStateTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(() -> mConnectionState.setVisibility(View.GONE));
            }
        }, 3000);
    }
}