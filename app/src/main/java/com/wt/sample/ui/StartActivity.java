package com.wt.sample.ui;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.pixplicity.easyprefs.library.Prefs;
import com.wt.sample.BuildConfig;
import com.wt.sample.R;
import com.wt.sample.utill.Utill;

public class StartActivity extends AppCompatActivity {

  public static final String DISPLAY_NAME_CODE = "display_name_code";

  private AppCompatEditText mDisplayName;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_start);

    ((AppCompatTextView) findViewById(R.id.app_version)).setText(BuildConfig.VERSION_NAME);

    mDisplayName = findViewById(R.id.display_name);
    mDisplayName.setText(Prefs.getString(DISPLAY_NAME_CODE, ""));

    // Click Listener for sign in button
    findViewById(R.id.sign_in).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View v) {
        checkMediaPermissions();
      }
    });

    // Done actions sign in button click
    mDisplayName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(final TextView v, final int actionId, final KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
          checkMediaPermissions();
          return true;
        }
        return false;
      }
    });
  }

  private void checkMediaPermissions() {
    if (ContextCompat.checkSelfPermission(this, RECORD_AUDIO) + ContextCompat.checkSelfPermission(this, CAMERA) != PERMISSION_GRANTED) {
      ActivityCompat.requestPermissions(this, new String[]{RECORD_AUDIO, CAMERA}, 1);
      return;
    }
    // Permissions granted
    signIn();
  }

  // Check if user granted media permissions for app
  @Override
  public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    if (requestCode == 1 && grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[0] == grantResults[1]) {
      signIn();
      return;
    }
    // Permissions not allowed
    onError("Camera and Microphone permissions not allowed");
  }

  private void signIn() {
    final String displayName = String.valueOf(mDisplayName.getText());
    Prefs.putString(DISPLAY_NAME_CODE, displayName);

    startMainActivity(displayName);
  }

  private void startMainActivity(String displayName) {
    final Intent intent = new Intent(StartActivity.this, MainActivity.class);
    intent.putExtra(DISPLAY_NAME_CODE, displayName);
    startActivity(intent);
  }

  private void onError(final String error) {
    new Handler().post(new Runnable() {
      @Override
      public void run() {
        Utill.showError(StartActivity.this, error);
      }
    });
  }
}
