package com.wt.sample.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.wt.sample.R;

import java.util.ArrayList;
import java.util.List;

public class ParticipantsAdapter extends RecyclerView.Adapter<ParticipantsAdapter.ViewHolder> {

    private final List<String> mParticipants = new ArrayList<>();

    ParticipantsAdapter() {
    }

    @NonNull
    @Override
    public ParticipantsAdapter.ViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, final int i) {
        return new ParticipantsAdapter.ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_participant, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ParticipantsAdapter.ViewHolder viewHolder, final int position) {
        final String publisher = mParticipants.get(viewHolder.getAdapterPosition());
        if (publisher != null) {
            viewHolder.order.setText(String.valueOf(viewHolder.getAdapterPosition() + 1));
            viewHolder.name.setText(publisher);
        }
    }

    @Override
    public int getItemCount() {
        return mParticipants.size();
    }

    void addParticipant(final String participantName) {
        mParticipants.add(participantName);
        notifyItemInserted(getItemCount());
    }

    void removeParticipant(final String participantName) {
        if (!mParticipants.isEmpty() && mParticipants.contains(participantName)) {
            final int positionToRemove = mParticipants.indexOf(participantName);
            mParticipants.remove(participantName);
            notifyItemRemoved(positionToRemove);
        }
    }

    void clearParticipants() {
        mParticipants.clear();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final AppCompatTextView order, name;

        ViewHolder(View v) {
            super(v);
            order = v.findViewById(R.id.order);
            name = v.findViewById(R.id.name);
        }
    }
}