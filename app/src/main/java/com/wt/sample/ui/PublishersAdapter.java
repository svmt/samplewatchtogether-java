package com.wt.sample.ui;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.wt.sample.R;
import com.wt.sdk.MediaConfiguration;
import com.wt.sdk.Participant;
import com.wt.sdk.ParticipantType;
import com.wt.sdk.VideoRenderer;

import java.util.ArrayList;
import java.util.List;

public class PublishersAdapter extends RecyclerView.Adapter<PublishersAdapter.ViewHolder> {

    private final List<Participant> mPublishers = new ArrayList<>();
    private final SwitchCamCallback mSwitchCamCallback;

    PublishersAdapter(final SwitchCamCallback callback) {
        mSwitchCamCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, final int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_publisher, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {
        final Participant participant = mPublishers.get(viewHolder.getAdapterPosition());
        if (participant != null) {
            participant.setVideoRenderer(viewHolder.videoRenderer);
            // Attaching participant's media stream to video renderer
            participant.attachStream();
            viewHolder.mic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    if (participant.isAudioEnabled()) {
                        participant.disableAudio();
                    } else {
                        participant.enableAudio();
                    }
                }
            });
            viewHolder.cam.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    if (participant.isVideoEnabled()) {
                        participant.disableVideo();
                    } else {
                        participant.enableVideo();
                    }
                }
            });

            viewHolder.connectionState.setVisibility(participant.isConnectionLost() ? View.VISIBLE : View.GONE);
            viewHolder.layoutProgress.setVisibility(participant.isProgressReconnection() ? View.VISIBLE : View.GONE);
            viewHolder.mic.setBackgroundResource(participant.isAudioEnabled() ? R.drawable.ic_mic_on : R.drawable.ic_mic_off);
            viewHolder.cam.setBackgroundResource(participant.isVideoEnabled() ? R.drawable.ic_video_on : R.drawable.ic_video_off);

            viewHolder.name.setText(participant.getName());

            // Check if its local participant
            viewHolder.switchCam.setVisibility(participant.isLocal() ? View.VISIBLE : View.GONE);
            viewHolder.volumeLevel.setVisibility(participant.isLocal() ? View.GONE : View.VISIBLE);

            // Check if its local participant
            if (viewHolder.getAdapterPosition() == 0) {
                viewHolder.switchCam.setVisibility(View.VISIBLE);
                viewHolder.volumeLevel.setVisibility(View.GONE);
                viewHolder.switchCam.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mSwitchCamCallback != null) {
                            mSwitchCamCallback.onSwitchCameraClicked();
                        }
                    }
                });
            } else {
                viewHolder.volumeLevel.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if (fromUser) {
                            participant.setVolumeLevel(progress);
                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return mPublishers.size();
    }

    void remotePublisherConnectionLost(String participantId) {
        for (int i = 0; i < mPublishers.size(); i++) {
            Participant participant = mPublishers.get(i);
            if (participant.getId().equals(participantId)) {
                int itemIndex = mPublishers.indexOf(participant);
                participant.setConnectionLost(true);
                notifyItemChanged(itemIndex, participant);
            }
        }
    }

    void updatePublisher(String publisherId, Participant publisher) {
        for (int i = 0; i < mPublishers.size(); i++) {
            Participant participant = mPublishers.get(i);
            if (participant.getId().equals(publisherId)) {
                int itemIndex = mPublishers.indexOf(participant);
                publisher.setVideoRenderer(participant.getVideoRenderer());
                publisher.setStreamAttached(false);
                publisher.setUpdateVideoRenderer(true);
                publisher.setAudioLocalEnabled(participant.isAudioLocalEnabled());
                publisher.setVideoLocalEnabled(participant.isVideoLocalEnabled());
                publisher.setAudioEnabled(participant.isAudioEnabled());
                publisher.setVideoEnabled(participant.isVideoEnabled());
                publisher.setConnectionLost(false);
                publisher.attachStream();
                mPublishers.set(itemIndex, publisher);
                notifyItemChanged(itemIndex, publisher);
            }
        }
    }

    void progressConnection(String publisherId, boolean isProgress) {
        for (int i = 0; i < mPublishers.size(); i++) {
            Participant participant = mPublishers.get(i);
            if (participant.getId().equals(publisherId)) {
                int itemIndex = mPublishers.indexOf(participant);
                participant.setProgressReconnection(isProgress);
                mPublishers.set(itemIndex, participant);
                notifyItemChanged(itemIndex, participant);
            }
        }
    }

    void addLocalPublisher(final Participant participant) {
        if (mPublishers.size() > 0) {
            if (mPublishers.get(0).isLocal()) {
                removePublisher(mPublishers.get(0));
            }
        }
        addPublisher(participant);
    }

    void addPublisher(final Participant participant) {
        if (participant.getType() == ParticipantType.FULL_PARTICIPANT || participant.getType() == ParticipantType.AV_BROADCASTER) {
            mPublishers.add(participant);
            notifyItemInserted(getItemCount() - 1);
        }
    }

    void removePublisher(final Participant participant) {
        if (!mPublishers.isEmpty() && mPublishers.contains(participant)) {
            final int positionToRemove = mPublishers.indexOf(participant);
            mPublishers.remove(participant);
            notifyItemRemoved(positionToRemove);
            if (participant != null) {
                participant.releaseRenderer();
            }
        }
    }

    void removePublisherById(final String publisherId) {
        for (Participant participant : mPublishers) {
            if (participant.getId().equals(publisherId)) {
                final int positionToRemove = mPublishers.indexOf(participant);
                mPublishers.remove(participant);
                notifyItemRemoved(positionToRemove);
                if (participant != null) {
                    participant.releaseRenderer();
                }
                break;
            }
        }
    }

    void clearPublishers() {
        for (final Participant participant : mPublishers) {
            if (participant != null && participant.getVideoRenderer() != null) {
                participant.releaseRenderer();
            }
        }
    }

    void updatePublisherMedia(final String participantId, final MediaConfiguration.MediaType mediaType, final MediaConfiguration.MediaState mediaState) {
        if (!mPublishers.isEmpty()) {
            for (int position = 0; position < mPublishers.size(); position++) {
                final Participant participant = mPublishers.get(position);
                if (participant != null) {
                    if (!TextUtils.isEmpty(participant.getId()) && !TextUtils.isEmpty(participantId) && participant.getId().equals(participantId)) {
                        switch (mediaType) {
                            case AUDIO:
                                participant.setAudioEnabled(mediaState);
                                break;
                            case VIDEO:
                                participant.setVideoEnabled(mediaState);
                                break;
                            default:
                                break;
                        }
                        notifyItemChanged(position, participant); // Update exactly view after its changed
                        break;
                    }
                }
            }
        }
    }

    interface SwitchCamCallback {
        void onSwitchCameraClicked();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final VideoRenderer videoRenderer;
        final AppCompatImageView mic, cam, switchCam;
        final AppCompatTextView name, connectionState;
        final SeekBar volumeLevel;
        final LinearLayout layoutProgress;

        ViewHolder(View v) {
            super(v);
            layoutProgress = v.findViewById(R.id.layout_progress);
            videoRenderer = v.findViewById(R.id.video_renderer);
            name = v.findViewById(R.id.participant_name);
            connectionState = v.findViewById(R.id.txt_connection_state);
            mic = v.findViewById(R.id.mic);
            cam = v.findViewById(R.id.cam);
            switchCam = v.findViewById(R.id.switch_cam);
            volumeLevel = v.findViewById(R.id.volume_level);
        }
    }
}