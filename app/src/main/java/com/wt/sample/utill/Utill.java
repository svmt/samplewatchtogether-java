package com.wt.sample.utill;

import android.content.Context;
import android.widget.Toast;

import com.wt.sample.R;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class Utill {

    public static void showError(final Context context, final String errorMessage) {
        showToast(context, errorMessage, android.R.color.holo_red_dark);
    }

    public static void showMessage(final Context context, final String errorMessage) {
        showToast(context, errorMessage, R.color.colorAccent);
    }

    private static void showToast(final Context context, final String errorMessage, final int textColorId) {
        final Toast t = Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT);
        t.show();
    }

    public static String formatDurationToHHMMSSSSS(long duration) {
        long seconds = TimeUnit.MILLISECONDS.toSeconds(duration);
        long remainingSeconds = Math.abs(seconds % 60);
        long mins = seconds / 60;
        long remainingMins = Math.abs(mins % 60);
        long hours = mins / 60;
        return String.format(Locale.US, "%d:%02d:%02d", hours, remainingMins, remainingSeconds);
    }
}
